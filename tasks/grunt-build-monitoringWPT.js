module.exports = function(grunt) {

    grunt.registerMultiTask('monitoringWPTOptions', 'Description', function() {

        // Tell grunt this task is asynchronous.
        var done = this.async();

        // Get the options received
        var options = this.options();
        var siteConfig = grunt.file.readJSON('config/siteConfig.json');
        var criteriaConfig = grunt.file.readJSON('PerformanceConfigs/criteriaconfig.json');
        var criteriaParam = null;
        if (siteConfig.siteConfig.performancecriteria == "V5"){
            criteriaParam = criteriaConfig.CriteriaConfig.V5;
        }
        if (siteConfig.siteConfig.performancecriteria == "V4"){
            criteriaParam = criteriaConfig.CriteriaConfig.V4;
        }
        // Define the tasks list
        var taskList = [];
        // -----------------------
        var wpttolog = {
            default: {
                options: {
                    instanceUrl: siteConfig.siteConfig.instance,
                    testUrl: siteConfig.siteConfig.url,
                    runs: siteConfig.siteConfig.runs,
                    location: siteConfig.siteConfig.location,
                    label: siteConfig.siteConfig.label,
                    private: siteConfig.siteConfig.private,
                    connectivity: siteConfig.siteConfig.connectivity,
                    video: siteConfig.siteConfig.video,
                    performancecriteria: criteriaParam
                }
            }
        };

        grunt.config.set('wpttolog', wpttolog);
        taskList.push('wpttolog');

        // ------------------------
        // RUN ALL TASKS
        grunt.task.run(taskList);

        done();

    });

};