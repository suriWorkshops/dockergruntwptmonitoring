module.exports = function(grunt) {

    // Load all node modules related to Grunt
    require('load-grunt-tasks')(grunt);

    // Init some params to run our tasks
    grunt.params = {
        styles: {
            project: {
                config:   '../../config.rb',
                sassDir:  '../../scss',
                cssDir:   '../../styles',
                sourcemap: false
            }
        }
    };
    // Project configuration.
    grunt.initConfig({
        monitoringWPTOptions: {
            project: {
                options: grunt.params.styles.project
            }
        },
        watch: {}
    });


    // Load in any and all tasks in the `grunt-tasks` folder
    grunt.loadTasks('tasks');

    // Build STYLES task
    grunt.registerTask('build-monitoringWPT', function() {
        grunt.task.run('monitoringWPTOptions:project');
    });

    // Default task
    grunt.registerTask('default', function() {
        grunt.task.run('build-monitoringWPT');
    });
};